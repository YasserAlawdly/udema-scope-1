<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = [
    'title', 'description', 'link', 'course_id', 'duration',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
}
