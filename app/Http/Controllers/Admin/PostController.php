<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('Dashboard.Posts.index' ,[
            'posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.Posts.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:191|unique:posts,title',
            'description'=>'required|max:191',
            'image'=>'sometimes|file|max:2048',
            'category_id'=>'required'
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }
        Post::create($inputs);
        return redirect(route('admin.posts.index'))
            ->with('success','Post Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('Dashboard.Posts.edit')
            ->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->validate($request,[
            'title'=>'required|max:191|unique:posts,title,'.$id,
            'description'=>'required|max:191',
            'image'=>'sometimes|file|max:2048',
            'category_id'=>'required'
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }

        $post->update($inputs);
        return redirect(route('admin.posts.index'))
            ->with('success','Post Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(!$post) return back()->with('error','Post Not Found');
        $post->delete();
        return redirect(route('admin.posts.index'))
            ->with('success','Post Deleted Succesfully');
    }
}

