<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserCourse;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function index(){

        $requests = UserCourse::all();
        return view('Dashboard.Requests.index' , [
            'requests' => $requests
        ]);
    }

    public function edit($id)
    {
        $request = UserCourse::findOrFail($id);
        return view('Dashboard.Requests.edit')
            ->with('request',$request);
    }

    public function update(Request $request, $id)
    {
        $approval = UserCourse::find($id);

        $approval->update($this->validate($request,[
            'approval'=>'required|boolean',
        ]));
        return redirect(route('admin.requests.index'))
            ->with('success','Request Updated Succesfully');
    }

    public function destroy($id)
    {
        $request = UserCourse::find($id);
        if(!$request) return back()->with('error','Request Not Found');
        $request->delete();
        return redirect(route('admin.requests.index'))
            ->with('success','Course Deleted Succesfully');
    }
}
