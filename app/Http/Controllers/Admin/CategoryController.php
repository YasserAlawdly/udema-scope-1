<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('Dashboard.Categories.index' , [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.Categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:191|unique:categories,name',
            'image'=>'sometimes|file|max:2048',
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploader($request,'image');
        }
        Category::create($inputs);
        return redirect(route('admin.categories.index'))
            ->with('success','Category Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('Dashboard.Categories.Edit')
            ->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|max:191|unique:categories,name,' .$id,
            'image'=>'nullable|file|max:2048',
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }

        $category->update($inputs);
        return redirect(route('admin.categories.index'))
            ->with('success','Category Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if(!$category) return back()->with('error','Category Not Found');
        $category->delete();
        return redirect(route('admin.categories.index'))
            ->with('success','Category Deleted Succesfully');
    }
}
