<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::all();
        return view('Dashboard.Lessons.index' , [
            'lessons' => $lessons
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.Lessons.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Lesson::create($this->validate($request,[
            'title'=>'required|max:191',
            'description'=>'required',
            'link'=>'required|max:191',
            'course_id'=>'required',
            'duration'=>'required|numeric|gt:0'
        ]));
        return redirect(route('admin.lessons.index'))
            ->with('success','Lesson Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson = Lesson::findOrFail($id);
        return view('Dashboard.Lessons.edit')
            ->with('lesson',$lesson);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson =Lesson::find($id);

        $lesson->update($this->validate($request,[
            'title'=>'required|max:191',
            'description'=>'required',
            'link'=>'required|max:191',
            'course_id'=>'required',
            'duration'=>'required|numeric|gt:0'
        ]));
        return redirect(route('admin.lessons.index'))
            ->with('success','Lesson Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        if(!$lesson) return back()->with('error','Lesson Not Found');
        $lesson->delete();
        return redirect(route('admin.lessons.index'))
            ->with('success','Lesson Deleted Succesfully');
    }
}
