<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();
        return view('Dashboard.Users.index' , [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Dashboard.Users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
        'name'=>'required|max:191',
        'email'=>'required|email|max:191|unique:users,email',
        'phone'=>'required|max:191|unique:users,phone',
        'password'=>'required|min:6|confirmed',
        'role'=>'required|in:admin,user',
        'is_active'=>'required|boolean',
        'image'=>'sometimes|file|max:2048',
    ]);
        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }
        User::create($inputs);
        return redirect(route('admin.users.index'))
            ->with('success','User Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::FindOrFail($id);
        $comments  = $user->comments;
        return view('Dashboard.Users.show',compact('user','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('Dashboard.Users.Edit')
            ->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|max:191',
            'email'=>'required|email|max:191|unique:users,email,'.$id,
            'phone'=>'required|max:191|unique:users,phone,'.$id,
            'password'=>'nullable|min:6|confirmed',
            'role'=>'required|in:admin,user',
            'is_active'=>'required|boolean',
            'image'=>'nullable|file|max:2048',
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }

        $user->update($inputs);
        return redirect(route('admin.users.index'))
            ->with('success','User Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!$user) {
            return back()->with('error', 'User Not Found');
        }
        if(auth()->user()->id == $user->id) return back()->with('error','You Cannont Delet You MemberShip');
        $user->delete();
        return redirect(route('admin.users.index'))
            ->with('success','User Deleted Succesfully');
    }
}
