<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('Dashboard.Courses.index' , [
            'courses' => $courses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.Courses.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:191|unique:courses,title',
            'short_description'=>'required|max:191|',
            'description'=>'required',
            'price'=>'required|numeric|min:0|not_in:0',
            'category_id'=>'required',
            'is_active'=>'required|boolean',
            'image'=>'sometimes|file|max:2048',
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }
        Course::create($inputs);
        return redirect(route('admin.courses.index'))
            ->with('success','Course Added Succesfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::FindOrFail($id);
        $lessons  = $course->lessons;
        return view('Dashboard.Courses.show',compact('course','lessons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        return view('Dashboard.Courses.edit')
            ->with('course',$course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);

        $this->validate($request,[
            'title'=>'required|max:191|unique:courses,title,'.$id,
            'short_description'=>'required|max:191|',
            'description'=>'required',
            'price'=>'required|numeric|min:0|not_in:0',
            'category_id'=>'required',
            'is_active'=>'required|boolean',
            'image'=>'sometimes|file|max:2048',
        ]);

        $inputs = $request->except(['image']);
        if($request->hasFile('image'))
        {
            $inputs['image']= uploadImg($request->image);
        }
        $course->update($inputs);
        return redirect(route('admin.courses.index'))
            ->with('success','Course Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        if(!$course) return back()->with('error','Course Not Found');
        $course->delete();
        return redirect(route('admin.courses.index'))
            ->with('success','Course Deleted Succesfully');
    }
}
