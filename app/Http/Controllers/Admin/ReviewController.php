<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::all();
        return view('Dashboard.Reviews.index' , [
            'reviews' => $reviews
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.Reviews.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Review::create($this->validate($request,[
            'user_id'=>'required',
            'course_id'=>'required',
            'comment'=>'required|max:191',
            'rate'=>'required|numeric|gt:0'
        ]));
        return redirect(route('admin.reviews.index'))
            ->with('success','Review Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::findOrFail($id);
        return view('Dashboard.Reviews.edit')
            ->with('review',$review);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $review = Review::find($id);

        $review->update($this->validate($request,[
            'user_id'=>'required',
            'course_id'=>'required',
            'comment'=>'required|max:191',
            'rate'=>'required|numeric|gt:0|in:5'
        ]));
        return redirect(route('admin.reviews.index'))
            ->with('success','Review Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Lesson::find($id);
        if(!$review) return back()->with('error','Review Not Found');
        $review->delete();
        return redirect(route('admin.lessons.index'))
            ->with('success','Review Deleted Succesfully');
    }
}
