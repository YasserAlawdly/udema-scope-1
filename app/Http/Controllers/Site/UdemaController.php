<?php

namespace App\Http\Controllers\Site;
use App\Category;
use App\Course;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class UdemaController extends Controller
{
    public function index(){

        $posts = Post::all();
        $courses = Course::latest()->limit(6)->get();
        $categories = Category::latest()->limit(3)->get();
        $recentPosts = Post::latest()->limit(4)->get();
        return view('Udema.index',[
            'posts'=>$posts,
            'recentPosts'=>$recentPosts,
            'categories'=>$categories,
            'courses' => $courses,
        ]);
    }




}
