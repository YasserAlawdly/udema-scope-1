<?php

namespace App\Http\Controllers\Site;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(){

        $courses = Course::latest()->paginate(3);

        return view('Udema.Course.index' , [
            'courses' => $courses
        ]);
    }

    public function show($id){

        $course = Course::find($id);
        $lessons = $course->lessons;
        $reviews = $course->reviews;
        return view('Udema.Course.show' , [
            'course' => $course,
            'lessons' => $lessons,
            'reviews' => $reviews
        ]);
    }

    public function course($id){

        $course = Course::find($id);
        $user = auth()->user()->id;

        $course->users()->attach($user);

        return redirect(route('course.show' ,['id'=>$course->id]))
            ->with('success','Request Progressed Succesfully');

    }
}
