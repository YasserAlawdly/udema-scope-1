<?php

namespace App\Http\Controllers\Site;

use App\Category;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index($category_id = null){

        if ($category_id) {
            $posts = Post::where('category_id', $category_id)->latest()->paginate(3);
        } else {
            $posts = Post::latest()->paginate(3);
        }
        $categories = Category::all();
        $recentPosts = Post::latest()->limit(3)->get();
        return view('Udema.Blog.index',[
            'posts'=>$posts,
            'categories'=>$categories,
            'recentPosts' =>$recentPosts
        ]);
    }

    public function show($id){
        $post = Post::find($id);
        $comments = $post->comments;
        $categories = Category::all();
        $recentPosts = Post::latest()->limit(3)->get();
        return view('Udema.Blog.show' , [
            'post' => $post,
            'categories'=>$categories,
            'recentPosts' =>$recentPosts,
            'comments' =>$comments
        ]);
    }

    public  function comment(Request $request,$id){
        $post = Post::find($id);

        $this->validate($request,[
            'comment' => 'required|max:191',
        ]);

        Comment::create([
            'comment' => $request->comment,
            'user_id' => auth()->user()->id,
            'post_id' => $post->id,
        ]);

        return redirect()->route('blog.show', ['id' => $post->id]);

    }
}
