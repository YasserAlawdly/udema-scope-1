<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|max:191',
            'email'=>'required|email|max:191|unique:users,email',
            'phone'=>'required|max:191|unique:users,phone',
            'password'=>'required|min:6|confirmed',
            'role'=>'required|in:admin,user',
            'is_active'=>'required|boolean',
            'image'=>'sometimes|file|max:2048',
        ];

        if ($this->method()=='PUT') {
            $rules = [
                'name'=>'required|max:191',
                'email'=> 'required|email|max:191|unique:users,email,'.$this->id,
                'phone'=> 'required|max:191|unique:users,phone,'.$this->id,
                'password'=>'nullable|min:6|confirmed',
                'role'=>'required|in:admin,user',
                'is_active'=>'required|boolean',
                'image'=>'nullable|file|max:2048',
            ];
        }
        return $rules;
    }
}
