<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'First_name', 'last_name', 'email', 'phone', 'message', 'user_id',
    ];
}
