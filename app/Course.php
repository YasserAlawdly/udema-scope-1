<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'title', 'short_description', 'description', 'price', 'image', 'category_id', 'is_active',
    ];

    protected $appends = ['total_duration'];

    public function category()
    {
        return $this->belongsTo(category::class,'category_id');
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class,'course_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class,'course_id');
    }

    public function getTotalDurationAttribute()
    {
        return $this->lessons->sum('duration');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_courses', 'course_id', 'user_id')->withPivot('approval');

    }

}
