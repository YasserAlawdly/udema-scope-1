<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'description', 'image', 'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(category::class,'category_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'post_id');
    }
}
