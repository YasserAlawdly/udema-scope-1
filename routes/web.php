<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'Site\UdemaController@index')->name('udema');
Route::get('/blog/{category_id?}', 'Site\BlogController@index')->name('blog');
Route::get('/blog/{id}/post', 'Site\BlogController@show')->name('blog.show');
Route::post('/blog/{id}', 'Site\BlogController@comment')->name('blog.store')->middleware('auth');
Route::get('/course', 'Site\CourseController@index')->name('course');
Route::get('/course/{id}', 'Site\CourseController@show')->name('course.show');
Route::post('/course/{id}', 'Site\CourseController@course')->name('course.store')->middleware('auth');


Route::group(['prefix'=>'dashboard','middleware'=>'admin','as'=>'admin.','namespace'=>'\App\Http\Controllers\Admin'],function(){

    Route::get('/','AdminMainContoller@index')->name('main');
    Route::resource('users','UserController');
    Route::resource('categories','CategoryController');
    Route::resource('posts','PostController');
    Route::resource('courses','CourseController');
    Route::resource('lessons','LessonController');
    Route::resource('reviews','ReviewController');
    Route::resource('comments','CommentController');
    Route::resource('requests','RequestController');
});

