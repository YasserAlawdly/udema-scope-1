 <!-- Favicons-->
    <link rel="shortcut icon" href="udema/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('/')}}udema/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('/')}}udema/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('/')}}udema/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('/')}}udema/img/apple-touch-icon-144x144-precomposed.png">

    <!-- BASE CSS -->
    <link href="{{asset('/')}}udema/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('/')}}udema/css/style.css" rel="stylesheet">
    <link href="{{asset('/')}}udema/css/vendors.css" rel="stylesheet">
    <link href="{{asset('/')}}udema/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('/')}}udema/css/custom.css" rel="stylesheet">


