@extends('Udema.layout')

@section('title')
    Home Page
@endsection

@section('content')

    <main>
        <section class="hero_single version_2">
            <div class="wrapper">
                <div class="container">
                    <h3>What would you learn?</h3>
                    <p>Increase your expertise in business, technology and personal development</p>
                    <form>
                        <div id="custom-search-input">
                            <div class="input-group">
                                <input type="text" class=" search-query" placeholder="Ex. Architecture, Specialization...">
                                <input type="submit" class="btn_search" value="Search">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /hero_single -->

        <div class="features clearfix">
            <div class="container">
                <ul>
                    <li><i class="pe-7s-study"></i>
                        <h4>+200 courses</h4><span>Explore a variety of fresh topics</span>
                    </li>
                    <li><i class="pe-7s-cup"></i>
                        <h4>Expert teachers</h4><span>Find the right instructor for you</span>
                    </li>
                    <li><i class="pe-7s-target"></i>
                        <h4>Focus on target</h4><span>Increase your personal expertise</span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /features -->

        <div class="container-fluid margin_120_0">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Udema Popular Courses</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div id="reccomended" class="owl-carousel owl-theme">
                @foreach($courses as $course)
                <div class="item">
                    <div class="box_grid">
                        <figure>
                            <a href="{{route('course.show' ,['id'=>$course->id])}}">
                                <div class="preview"><span>Preview course</span></div><img src="{{getimg($course->image)}}" class="img-fluid" alt=""></a>
                            <div class="price">{{'$' . $course->price}}</div>

                        </figure>
                        <div class="wrapper">
                            <small>{{$course->category->name}}</small>
                            <h3>{{$course->short_description}}</h3>
                            <p>{{$course->description}}</p>
                            <div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>{{$course->reviews->count()}}</small></div>
                        </div>
                        <ul>
                            <li><i class="icon_clock_alt"></i>{{' ' . $course->getTotalDurationAttribute() . ' : min'}}</li>
                            <li><a href="{{route('course.show' ,['id'=>$course->id])}}">Details</a></li>
                        </ul>
                    </div>
                </div>
                @endforeach
                <!-- /item -->

                <!-- /item -->

                <!-- /item -->

                <!-- /item -->

                <!-- /item -->

                <!-- /item -->
            </div>
            <!-- /carousel -->
            <div class="container">
                <p class="btn_home_align"><a href="/course" class="btn_1 rounded">View all courses</a></p>
            </div>
            <!-- /container -->
            <hr>
        </div>
        <!-- /container -->

        <div class="container margin_30_95">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Udema Categories Courses</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div class="row">
                @foreach($categories as $category)
                <div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
                    <a href="#0" class="grid_item">
                        <figure class="block-reveal">
                            <div class="block-horizzontal"></div>
                            <img src="{{getimg($category->image)}}" class="img-fluid" alt="">
                            <div class="info">
                                <small><i class="ti-layers"></i>{{$category->courses->count() . ' courses'}} </small>
                                <h3>{{$category->name}}</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                @endforeach
                <!-- /grid_item -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->

        <div class="bg_color_1">
            <div class="container margin_120_95">
                <div class="main_title_2">
                    <span><em></em></span>
                    <h2>News and Events</h2>
                    <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                </div>
                <div class="row">
                    @foreach($recentPosts as $post)
                    <div class="col-lg-6">
                        <a class="box_news" href="{{route('blog.show',['id'=>$post->id])}}">
                            <figure><img src="{{getimg($post->image)}}" alt=""></figure>
                            <ul>
                                <li>{{$post->create_at}}</li>
                            </ul>
                            <h4>{{$post->title}}</h4>
                            <p>{{$post->description}}</p>
                        </a>
                    </div>
                    @endforeach
                </div>
                <!-- /row -->
                <p class="btn_home_align"><a href="{{route('blog')}}" class="btn_1 rounded">View all news</a></p>
            </div>
            <!-- /container -->
        </div>
        <!-- /bg_color_1 -->

    </main>
    <!-- /main -->




@endsection
