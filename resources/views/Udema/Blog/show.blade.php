@extends('Udema.layout')

@section('title')
    Blog Page
@endsection

@section('css')
    <link href="{{asset('/')}}udema/css/blog.css" rel="stylesheet">
@endsection

@section('content')
    <main>
        <section id="hero_in" class="general">
            <div class="wrapper">
                <div class="container">
                    <h1 class="fadeInUp"><span></span>Udema blog</h1>
                </div>
            </div>
        </section>
        <!--/hero_in-->

        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-9">
                    <div class="bloglist singlepost">
                        <p><img alt="" class="img-fluid"
                                src="{{getimg($post->image)}}"></p>
                        <h1>{{$post->title}}</h1>
                        <div class="postmeta">
                            <ul>
                                <li><a href="#"><i class="icon_folder-alt"></i> {{$post->category->name}}</a></li>
                                <li><a href="#"><i class="icon_clock_alt"></i> {{$post->created_at}}</a></li>
                                <li><a href="#"><i class="icon_comment_alt"></i> {{$post->comments->count()}}</a></li>
                            </ul>
                        </div>
                        <!-- /post meta -->
                        <div class="post-content">

                            <p>{{$post->description}}</p>
                        </div>
                        <!-- /post -->
                    </div>
                    <!-- /single-post -->

                    <div id="comments">
                        <h5>Comments</h5>
                        <ul>
                            @foreach($comments as $comment)
                            <li>
                                <div class="avatar">
                                    <a href="#"><img src="{{getimg($comment->user->image)}}"
                                                     alt="">
                                    </a>
                                </div>

                                <div class="comment_right clearfix">
                                    <div class="comment_info">
                                        By <a href="#">{{$comment->user->name}}</a><span>|</span>{{$comment->created_at}}
                                    </div>
                                    <p>
                                        {{$comment->comment}}
                                    </p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <hr>

                    <h5>Leave a Comment</h5>
                    {!! Form::open(['route' => ['blog.store' , $post->id], 'method' => 'post','files'=>true]) !!}

                        <div class="form-group">
                            {!! Form::textarea('comment',null,['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" id="submit2" class="btn_1 rounded add_bottom_30"> Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <!-- /col -->

                <aside class="col-lg-3">
                    <div class="widget">
                        <form>
                            <div class="form-group">
                                <input type="text" name="search" id="search" class="form-control"
                                       placeholder="Search...">
                            </div>
                            <button type="submit" id="submit" class="btn_1 rounded"> Search</button>
                        </form>
                    </div>
                    <!-- /widget -->
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Recent Posts</h4>
                        </div>
                        <ul class="comments-list">
                            @foreach($recentPosts as $post)
                                <li>
                                    <div class="alignleft">
                                        <a href="{{route('blog.show' ,['id' => $post->id])}}"><img src="{{getimg($post->image)}}" alt=""></a>
                                    </div>
                                    <small>{{$post->created_at}}</small>
                                    <h3><a href="{{route('blog.show' ,['id' => $post->id])}}" title="">{{$post->title}}</a></h3>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /widget -->
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Blog Categories</h4>
                        </div>
                        <ul class="cats">
                            @foreach($categories as $category)
                                <li><a href="{{route('blog', $category->id)}}">{{$category->name}}
                                        <span>{{$category->posts->count()}}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /widget -->

                    <!-- /widget -->
                </aside>
                <!-- /aside -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </main>
    <!--/main-->
@endsection

