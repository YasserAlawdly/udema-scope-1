@extends('Udema.layout')

@section('title')
    Blog Page
@endsection

@section('css')
    <link href="{{asset('/')}}udema/css/blog.css" rel="stylesheet">
@endsection

@section('content')

    <main>
        <section id="hero_in" class="general">
            <div class="wrapper">
                <div class="container">
                    <h1 class="fadeInUp"><span></span>Udema blog</h1>
                </div>
            </div>
        </section>
        <!--/hero_in-->

        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-9">
                    @foreach($posts as $post)
                    <article class="blog wow fadeIn">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                <figure>
                                    <a href="{{route('blog.show' ,['id' => $post->id])}}"><img src="{{getimg($post->image)}}" alt="">
                                        <div class="preview"><span>Read more</span></div>
                                    </a>
                                </figure>
                            </div>
                            <div class="col-lg-5">
                                <div class="post_info">
                                    <small>{{$post->created_at}}</small>
                                    <h3><a href="{{route('blog.show' ,['id' => $post->id])}}">{{$post->title}}</a></h3>
                                    <p>{{$post->description}}</p>
                                    <ul>
                                        <li>
                                            <div class="thumb"><img src="{{getimg($post->category->image)}}" alt=""></div> {{$post->category->name}}
                                        </li>
                                        <li><i class="icon_comment_alt"></i> {{$post->comments->count()}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endforeach
                    <!-- /article -->

                    <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            {{$posts->links()}}
                        </ul>
                    </nav>
                    <!-- /pagination -->
                </div>
                <!-- /col -->

                <aside class="col-lg-3">
                    <div class="widget">
                        <form>
                            <div class="form-group">
                                <input type="text" name="search" id="search" class="form-control" placeholder="Search...">
                            </div>
                            <button type="submit" id="submit" class="btn_1 rounded"> Search</button>
                        </form>
                    </div>
                    <!-- /widget -->
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Recent Posts</h4>
                        </div>
                        <ul class="comments-list">
                            @foreach($recentPosts as $post)
                            <li>
                                <div class="alignleft">
                                    <a href="{{route('blog.show' ,['id' => $post->id])}}"><img src="{{getimg($post->image)}}" alt=""></a>
                                </div>
                                <small>{{$post->created_at}}</small>
                                <h3><a href="{{route('blog.show' ,['id' => $post->id])}}" title="">{{$post->title}}</a></h3>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /widget -->
                    <div class="widget">
                        <div class="widget-title">
                            <h4>Blog Categories</h4>
                        </div>
                        <ul class="cats">
                            @foreach($categories as $category)
                            <li><a href="{{route('blog', $category->id)}}">{{$category->name}} <span>{{$category->posts->count()}}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /widget -->

                    <!-- /widget -->
                </aside>
                <!-- /aside -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </main>
    <!--/main-->

@endsection
