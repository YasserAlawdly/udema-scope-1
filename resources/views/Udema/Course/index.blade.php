@extends('Udema.layout')

@section('title')
    Courses Page
@endsection

@section('content')

    <main>
    <section id="hero_in" class="courses">
        <div class="wrapper">
            <div class="container">
                <h1 class="fadeInUp"><span></span>Online courses</h1>
            </div>
        </div>
    </section>
    <!--/hero_in-->

    <!-- /filters -->

    <div class="container margin_60_35">
        <div class="row">
            @foreach($courses as $course)
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="box_grid wow">
                    <figure class="block-reveal">
                        <div class="block-horizzontal"></div>
                        <a href="{{route('course.show' ,['id'=>$course->id])}}"><img src="{{getimg($course->image)}}" class="img-fluid" alt=""></a>
                        <div class="price">{{'$' . $course->price}}</div>
                        <div class="preview"><span>Preview course</span></div>
                    </figure>
                    <div class="wrapper">
                        <small>{{$course->category->name}}</small>
                        <h3>{{$course->short_description}}</h3>
                        <p>{{$course->description}}</p>
                        <div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>{{$course->reviews->count()}}</small></div>
                    </div>
                    <ul>
                        <li><i class="icon_clock_alt"></i>{{' ' . $course->getTotalDurationAttribute() . ':min'}}</li>
                        <li><a href="{{route('course.show' ,['id'=>$course->id])}}">Details</a></li>
                    </ul>
                </div>
            </div>
            @endforeach

        </div>
        <!-- /row -->
        <nav aria-label="...">
            <ul class="pagination pagination-sm">
                {{$courses->links()}}
            </ul>
        </nav>
    </div>
    <!-- /container -->

    <!-- /bg_color_1 -->
</main>
<!--/main-->


