@extends('Dashboard.layout')

@section('title')

Users

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Users Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Users And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
                <tr>
                    <th>#</th>
                    <th>name</th>
                    <th>email</th>
                    <th>phone</th>
                    <th>role</th>
                    <th>comments</th>
                    <th>Active</th>
                    <th>operations</th>


                </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td><a href="#">{{$user->name}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>

                    <td><span class="label label-success">{{$user->role}}</span></td>
                    <td><a href="{{route('admin.users.show' , ['id' => $user->id])}}">{{$user->comments->count()}}</a></td>
                    <td><span class="label label-success">{{status()[$user->is_active]}}</span></td>
                    <td>
                        <a href="{{route('admin.users.edit',['id'=>$user->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.users.destroy',$user->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection
