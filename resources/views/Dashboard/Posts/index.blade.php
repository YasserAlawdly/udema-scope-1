@extends('Dashboard.layout')

@section('title')

    Posts

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Posts Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Posts And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>image</th>
                <th>description</th>
                <th>Category</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>
                        <div class="media no-margin-top">
                            @if(isset($post))
                                @if($post->image!=null)
                                    <div class="media-left">
                                        <a href="#"><img src="{{getimg($post->image)}}"
                                                         style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </td>
                    <td>{{$post->description}}</td>
                    <td>{{$post->category->name}}</td>
                    <td>
                        <a href="{{route('admin.posts.edit',['id'=>$post->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.posts.destroy',$post->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection


