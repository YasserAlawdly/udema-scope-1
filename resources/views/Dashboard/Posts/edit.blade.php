@extends('Dashboard.layout')

@section('title')
    Edit {{$post->title}}
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($post,['route' => ['admin.posts.update' ,$post->id] ,'method' => 'put' , 'files' => true]) !!}
        @include('Dashboard.Posts.form')
        {!! Form::close() !!}
    </div>
@endsection


