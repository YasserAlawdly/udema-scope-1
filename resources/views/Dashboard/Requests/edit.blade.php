@extends('Dashboard.layout')

@section('title')
    Edit Request
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($request,['route' => ['admin.requests.update' ,$request->id] ,'method' => 'put' , 'files' => true]) !!}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    @if(isset($request))
                        <h6 class="panel-title">Edit Request</h6>
                    @else
                        <h6 class="panel-title">Add New Request</h6>
                    @endif
                    <div class="heading-elements">

                    </div>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <label>Approval: </label>
                        {!! Form::select('approval', approval(), null, ['placeholder' => 'Select Request Approval']) !!}
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection



