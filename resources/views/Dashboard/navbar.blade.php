<!-- Main -->
<li class="navigation-header"><span>Control Panel</span> <i class="icon-menu" title="Main pages"></i></li>

{{--User Navbar--}}
<li class="active"><a href="{{route('admin.main')}}"><i class="icon-home4"></i> <span>Main</span></a></li>
<li>
    <a href="#"><i class="icon-users"></i> <span>Users Control</span></a>
    <ul>
        <li><a href="{{route('admin.users.index')}}">All Users</a></li>
        <li><a href="{{route('admin.users.create')}}">Create New User</a></li>
    </ul>
</li>

{{--Category Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Categories</span></a>
    <ul>
        <li><a href="{{route('admin.categories.index')}}">All Categories</a></li>
        <li><a href="{{route('admin.categories.create')}}">Create New Category</a></li>
    </ul>
</li>

{{--post Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Posts</span></a>
    <ul>
        <li><a href="{{route('admin.posts.index')}}">All Posts</a></li>
        <li><a href="{{route('admin.posts.create')}}">Create New Post</a></li>
    </ul>
</li>

{{--comment Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Comments</span></a>
    <ul>
        <li><a href="{{route('admin.comments.index')}}">All Comments</a></li>
        <li><a href="{{route('admin.comments.create')}}">Create New Comment</a></li>
    </ul>
</li>

{{--course Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Courses</span></a>
    <ul>
        <li><a href="{{route('admin.courses.index')}}">All Courses</a></li>
        <li><a href="{{route('admin.courses.create')}}">Create New Course</a></li>
    </ul>
</li>

{{--lesson Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Lessons</span></a>
    <ul>
        <li><a href="{{route('admin.lessons.index')}}">All Lessons</a></li>
        <li><a href="{{route('admin.lessons.create')}}">Create New Lesson</a></li>
    </ul>
</li>


{{--Review Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Reviews</span></a>
    <ul>
        <li><a href="{{route('admin.reviews.index')}}">All Reviews</a></li>
        <li><a href="{{route('admin.reviews.create')}}">Create New Review</a></li>
    </ul>
</li>

{{--Review Navbar--}}
<li>
    <a href="#"><i class="icon-cart"></i> <span>Requests</span></a>
    <ul>
        <li><a href="{{route('admin.requests.index')}}">All Request</a></li>
    </ul>
</li>
