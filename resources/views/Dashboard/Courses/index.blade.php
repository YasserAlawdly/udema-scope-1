@extends('Dashboard.layout')

@section('title')

    Courses

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Courses Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Courses And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>short_description</th>
                <th>description</th>
                <th>price</th>
                <th>lessons</th>
                <th>duration</th>
                <th>image</th>
                <th>Category</th>
                <th>Active</th>
                <th>Operations</th>


            </tr>
            </thead>
            <tbody>
            @foreach ($courses as $course)
                <tr>
                    <td>{{$course->id}}</td>
                    <td>{{$course->title}}</td>
                    <td>{{$course->short_description}}</td>
                    <td>{{$course->description}}</td>

                    <td><span class="label label-success">{{$course->price}}</span></td>
                    <td><a href="{{route('admin.courses.show' , ['id' => $course->id])}}">{{$course->lessons->count()}}</a></td>
                    <td>{{$course->getTotalDurationAttribute()}}</td>
                    <td>
                        <div class="media no-margin-top">
                            @if(isset($course))
                                @if($course->image!=null)
                                    <div class="media-left">
                                        <a href="#"><img src="{{getimg($course->image)}}"
                                                         style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </td>
                    <td>{{$course->category->name}}</td>
                    <td><span class="label label-success">{{status()[$course->is_active]}}</span></td>
                    <td>
                        <a href="{{route('admin.courses.edit',['id'=>$course->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.courses.destroy',$course->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection

