@extends('Dashboard.layout')

@section('title')
    Create New Course
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'admin.courses.store', 'method' => 'post','files'=>true]) !!}
        @include('Dashboard.Courses.form')
        {!! Form::close() !!}
    </div>
@endsection

