@extends('Dashboard.layout')

@section('title')
    Edit {{$course->title}}
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($course,['route' => ['admin.courses.update' ,$course->id] ,'method' => 'put' , 'files' => true]) !!}
        @include('Dashboard.Courses.form')
        {!! Form::close() !!}
    </div>
@endsection
