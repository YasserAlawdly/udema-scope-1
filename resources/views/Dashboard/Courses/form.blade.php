
<div class="panel panel-flat">
    <div class="panel-heading">
        @if(isset($course))
            <h6 class="panel-title">Edit {{$course->title}}</h6>
        @else
            <h6 class="panel-title">Add New Course</h6>
        @endif
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="form-group">
            <label>Course title: </label>
            {!! Form::text('title',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Course ShortDescription: </label>
            {!! Form::text('short_description',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Course description: </label>
            {!! Form::textarea('description',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Course price: </label>
            {!! Form::number('price',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Course Categories: </label>
            {!! Form::select('category_id', categories(), null, ['placeholder' => 'Select Course category']) !!}
        </div>

        <div class="form-group">
            <label>Select Status: </label>
            {!! Form::select('is_active', status(), null, ['placeholder' => 'Select Course status']) !!}
        </div>

        <div class="form-group col-md-12">
            <label class="text-semibold"> Course Image : </label>
            <div class="media no-margin-top">
                @if(isset($course))
                    @if($course->image!=null)
                        <div class="media-left">
                            <a href="#"><img src="{{getimg($course->image)}}" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
                        </div>
                    @endif
                @endif

                <div class="media-body">
                    <input type="file" class="file-styled" name="image" data-height="200">
                    <span class="help-block">الصيغ المسموح بها : gif, png, jpg , jpeg</span>
                </div>
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
        </div>
    </div>
</div>

