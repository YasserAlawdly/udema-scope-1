@extends('Dashboard.layout')

@section('title')
    Show {{$course->title}} Lessons
@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Show {{$course->title}} Lessons</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All {{$course->title}} Lessons And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>description</th>
                <th>link</th>
                <th>Course name</th>
                <th>duration</th>
            </tr>
            </thead>
            <tbody>
            @foreach($lessons as $lesson)
                <tr>
                    <td>{{$lesson->id}}</td>
                    <td>{{$lesson->title}}</td>
                    <td>{{$lesson->description}}</td>
                    <td>{{$lesson->link}}</td>
                    <td>{{$lesson->course->title}}</td>
                    <td>{{$lesson->duration}}</td>
                </tr>
            @endforeach



            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->





@endsection
