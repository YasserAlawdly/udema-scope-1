<div class="panel panel-flat">
    <div class="panel-heading">
        @if(isset($review))
            <h6 class="panel-title">Edit Review</h6>
        @else
            <h6 class="panel-title">Add New Review</h6>
        @endif
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="form-group">
            <label>Review comment: </label>
            {!! Form::text('comment',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Review Rate: </label>
            {!! Form::number('rate',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Review Categories: </label>
            {!! Form::select('course_id', courses(), null, ['class' => 'form-control','placeholder' => 'Select Review category']) !!}
        </div>

        <div class="form-group">
            <label>Review Users: </label>
            {!! Form::select('user_id', users(), null, ['class' => 'form-control','placeholder' => 'Select Review user']) !!}
        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
        </div>
    </div>
</div>



