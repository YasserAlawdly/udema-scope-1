@extends('Dashboard.layout')

@section('title')
    Create New Review
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'admin.reviews.store', 'method' => 'post','files'=>true]) !!}
        @include('Dashboard.Reviews.form')
        {!! Form::close() !!}
    </div>
@endsection

