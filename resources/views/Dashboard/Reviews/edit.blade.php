@extends('Dashboard.layout')

@section('title')
    Edit Review
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($review,['route' => ['admin.reviews.update' ,$review->id] ,'method' => 'put' , 'files' => true]) !!}
        @include('Dashboard.Reviews.form')
        {!! Form::close() !!}
    </div>
@endsection
