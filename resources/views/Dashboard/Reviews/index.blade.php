@extends('Dashboard.layout')

@section('title')

    Reviews

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Reviews Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Reviews And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>UserName</th>
                <th>CourseName</th>
                <th>Comment</th>
                <th>Rate</th>
                <th>Date</th>
                <th>Operation</th>

            </tr>
            </thead>
            <tbody>
            @foreach ($reviews as $review)
                <tr>
                    <td>{{$review->id}}</td>
                    <td>{{$review->user->name}}</td>
                    <td>{{$review->course->title}}</td>
                    <td>{{$review->comment}}</td>
                    <td>{{$review->rate}}</td>
                    <td>{{$review->created_at}}</td>
                    <td>
                        <a href="{{route('admin.reviews.edit',['id'=>$review->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.reviews.destroy',$review->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection

