
<div class="panel panel-flat">
    <div class="panel-heading">
        @if(isset($category))
            <h6 class="panel-title">Edit {{$category->name}}</h6>
        @else
            <h6 class="panel-title">Add New Category</h6>
        @endif
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="form-group">
            <label>Category name: </label>
            {!! Form::text('name',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-12">
            <label class="text-semibold"> Category Image : </label>
            <div class="media no-margin-top">
                @if(isset($category))
                    @if($category->image!=null)
                        <div class="media-left">
                            <a href="#"><img src="{{getimg($category->image)}}" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
                        </div>
                    @endif
                @endif

                <div class="media-body">
                    <input type="file" class="file-styled" name="image" data-height="200">
                    <span class="help-block">الصيغ المسموح بها : gif, png, jpg , jpeg</span>
                </div>
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
        </div>
    </div>
</div>

