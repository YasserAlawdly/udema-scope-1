@extends('Dashboard.layout')

@section('title')

    Comments

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Comments Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Comments And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>UserName</th>
                <th>PostTitle</th>
                <th>Comment</th>
                <th>Date</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($comments as $comment)
                <tr>
                    <td>{{$comment->id}}</td>

                    <td>{{$comment->users->name}}</td>
                    <td>{{$comment->posts->title}}</td>
                    <td>{{$comment->comment}}</td>
                    <td>{{$comment->created_at}}</td>
                    <td>
                        <a href="{{route('admin.comments.edit',['id'=>$comment->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.comments.destroy',$comment->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection

