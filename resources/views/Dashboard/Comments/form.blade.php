<div class="panel panel-flat">
    <div class="panel-heading">
        @if(isset($review))
            <h6 class="panel-title">Edit Comment</h6>
        @else
            <h6 class="panel-title">Add New Comment</h6>
        @endif
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="form-group">
            <label>comment: </label>
            {!! Form::text('comment',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>comment Posts: </label>
            {!! Form::select('post_id', posts(), null, ['class' => 'form-control','placeholder' => 'Select Comment Post']) !!}
        </div>

        <div class="form-group">
            <label>comment Users: </label>
            {!! Form::select('user_id', users(), null, ['class' => 'form-control','placeholder' => 'Select Comment user']) !!}
        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
        </div>
    </div>
</div>



