@extends('Dashboard.layout')

@section('title')
    Edit Comment
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($comment,['route' => ['admin.comments.update' ,$comment->id] ,'method' => 'put' , 'files' => true]) !!}
        @include('Dashboard.Comments.form')
        {!! Form::close() !!}
    </div>
@endsection
