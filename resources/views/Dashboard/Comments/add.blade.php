@extends('Dashboard.layout')

@section('title')
    Create New Comment
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'admin.comments.store', 'method' => 'post','files'=>true]) !!}
        @include('Dashboard.Comments.form')
        {!! Form::close() !!}
    </div>
@endsection
