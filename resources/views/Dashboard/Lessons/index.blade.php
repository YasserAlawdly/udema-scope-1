@extends('Dashboard.layout')

@section('title')

    Lessons

@endsection

@section('content')
    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Lessons Operations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            You Can Show All Lessons And performs All operations like search , Edit , show And Delete
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>description</th>
                <th>link</th>
                <th>Course name</th>
                <th>duration</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($lessons as $lesson)
                <tr>
                    <td>{{$lesson->id}}</td>
                    <td>{{$lesson->title}}</td>
                    <td>{{$lesson->description}}</td>
                    <td><a href="{{$lesson->link}}">Watch now</a></td>
                    <td><span class="label label-success">{{$lesson->course->title}}</span></td>
                    <td>{{$lesson->duration}}</td>
                    <td>
                        <a href="{{route('admin.lessons.edit',['id'=>$lesson->id])}}" class="btn btn-default">
                            <i class="icon-pencil"></i>
                        </a>

                        <form action="{{route('admin.lessons.destroy',$lesson->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="icon-trash"></i>
                            </button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

@endsection

