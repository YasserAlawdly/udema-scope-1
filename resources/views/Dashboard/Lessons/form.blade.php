
<div class="panel panel-flat">
    <div class="panel-heading">
        @if(isset($lesson))
            <h6 class="panel-title">Edit {{$lesson->title}}</h6>
        @else
            <h6 class="panel-title">Add New Lesson</h6>
        @endif
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="form-group">
            <label>Lesson title: </label>
            {!! Form::text('title',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Lesson description: </label>
            {!! Form::textarea('description',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Lesson link: </label>
            {!! Form::text('link',null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Course Categories: </label>
            {!! Form::select('course_id', courses(), null, ['class' => 'form-control','placeholder' => 'Select Course title']) !!}
        </div>

        <div class="form-group">
            <label>Lesson duration: </label>
            {!! Form::number('duration',null,['class' => 'form-control']) !!}

        </div>

        <div class="text-right">
            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-left13 position-right"></i></button>
        </div>
    </div>
</div>


