@extends('Dashboard.layout')

@section('title')
    Create New Lesson
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'admin.lessons.store', 'method' => 'post','files'=>true]) !!}
        @include('Dashboard.Lessons.form')
        {!! Form::close() !!}
    </div>
@endsection
