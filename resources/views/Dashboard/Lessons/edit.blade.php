@extends('Dashboard.layout')

@section('title')
    Edit {{$lesson->title}}
@endsection

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::model($lesson,['route' => ['admin.lessons.update' ,$lesson->id] ,'method' => 'put' , 'files' => true]) !!}
        @include('Dashboard.Lessons.form')
        {!! Form::close() !!}
    </div>
@endsection
