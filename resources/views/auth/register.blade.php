@extends('auth.layout')


@section('content')

<body id="register_bg">

<nav id="menu" class="fake_menu"></nav>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div>
<!-- End Preload -->

<div id="login">
    <aside>
        <figure>
            <a href="index.html"><img src="udema/img/logo.png" width="149" height="42" data-retina="true" alt=""></a>
        </figure>
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group">

					<span class="input">
					<input class="input_field @error('name') is-invalid @enderror" type="text" name="name"
                           value="{{ old('name') }}" required autocomplete="name" autofocus>
					@error('name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
                    @enderror
					<label class="input_label">
						<span class="input__label-content">{{ __('Name') }}</span>
					</label>
					</span>

                <span class="input">
					<input class="input_field @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" type="email">
					@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						<label class="input_label">
						<span class="input__label-content">{{ __('E-Mail Address') }}</span>
					</label>
					</span>

                <span class="input">
					<input class="input_field @error('phone') is-invalid @enderror" name="phone"
                           value="{{ old('email') }}" required autocomplete="phone" type="tel">
					@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						<label class="input_label">
						<span class="input__label-content">{{ __('Phone') }}</span>
					</label>
					</span>

                <span class="input">
					<input class="input_field @error('password') is-invalid @enderror" name="password" required
                           autocomplete="new-password" type="password" id="password1">
					@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
						<label class="input_label">
						<span class="input__label-content">{{ __('Password') }}</span>
					</label>
					</span>

                <span class="input">
					<input class="input_field" type="password" id="password_confirmation" name="password_confirmation"
                           required autocomplete="new-password">
						<label class="input_label">
						<span class="input__label-content">{{ __('Confirm Password') }}</span>
					</label>
					</span>

                <div id="pass-info" class="clearfix"></div>
            </div>
            <button type="submit" class="btn_1 rounded full-width add_top_30">{{ __('Register to Udema') }}</button>
            <div class="text-center add_top_10">Already have an acccount? <strong><a href="{{ route('login') }}">Sign
                        In</a></strong></div>
        </form>

<!-- /login -->

@endsection
