@extends('auth.layout')

@section('content')

<body id="login_bg">

<nav id="menu" class="fake_menu"></nav>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div>
<!-- End Preload -->

<div id="login">
    <aside>
        <figure>
            <a href="index.html"><img src="udema/img/logo.png" width="149" height="42" data-retina="true" alt=""></a>
        </figure>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
            <span class="input">
					<input class="input_field @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" type="email" autofocus>
					@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						<label class="input_label">
						<span class="input__label-content">{{ __('E-Mail Address') }}</span>
					</label>
					</span>

                <span class="input">
						<input class="input_field @error('password') is-invalid @enderror" name="password" required
                               autocomplete="current-password" type="password" id="password1">
						@error('password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
                        @enderror
							<label class="input_label">
							<span class="input__label-content">{{ __('Password') }}</span>
						</label>
						</span>
                @if (Route::has('password.request'))
                    <small><a href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a></small>
                @endif
            </div>

            <button type="submit" class="btn_1 rounded full-width add_top_60">{{ __('Login to Udema') }}</button>

            <div class="text-center add_top_10">New to Udema? <strong><a href="{{route('register')}}">Sign up!</a></strong>
            </div>
        </form>

<!-- /login -->

@endsection
